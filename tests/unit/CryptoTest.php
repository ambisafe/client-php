<?php
namespace Ambisafe\Tests;

use Ambisafe\Crypto;

/**
 * Class CryptoTest
 * @package Ambisafe\Tests
 */
class CryptoTest extends \PHPUnit_Framework_TestCase
{
    public function testPbkdf2()
    {
        $this->assertEquals('afe6c5530785b6cc6b1c6453384731bd5ee432ee549fd42fb6695779ad8a1c5b', unpack('H*', Crypto::deriveKey('salt', 'password'))[1]);
    }

    public function testEncryptionDecryption()
    {
        $data = Crypto::generateUuid();
        list($iv, $encrypted) = Crypto::encrypt($data, 'salt', 'password');
        $decrypted = Crypto::decrypt($encrypted, 'salt', $iv, 'password');
        $this->assertTrue(strcmp($data, $decrypted) == 0, 'Decrypted data is not equal to original!');
    }

    public function testDecryptionOfDataFromRuby()
    {
        $iv = hex2bin('73e7b8be4211436f81fc30f808a75cc6');
        $encrypted = hex2bin('426a10ce6e1709f8697422c33ad7cf48');
        $decrypted = Crypto::decrypt($encrypted, 'salt', $iv, 'password');
        $this->assertTrue(strcmp('krabutnizza 0', $decrypted) == 0, 'Decrypted data is not equal to the original!');
    }
}
