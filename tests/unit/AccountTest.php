<?php
namespace Ambisafe\Tests;

use Ambisafe\Client;
use Ambisafe\Container;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;


class AccountTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Ambisafe\Client
     */
    public $client;

    public function setUp()
    {
        $this->client = new Client('http://104.155.86.243:8080', 'ololo', 'demo', 'demo');
    }

    public function testCreateSimpleAccount()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/get_simple_account_response'))
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);

        $account_to_create = new \Ambisafe\Account\Simple(['id' => 1, 'currency' => 'BTC']);
        $account = $this->client->createAccount($account_to_create);

        $this->assertInstanceOf('\\Ambisafe\\Account\\Simple', $account);
        $this->assertEquals(1, $account->getId());
        $this->assertEquals('3MLbmeUjhiU8Artq2Xp3cdKZzXmocQahpy', $account->getAddress());
    }


    public function testCreateWallet4Account()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/get_wallet4_account_response'))
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);

        $account_to_create = new \Ambisafe\Account\Wallet4(['id' => 2, 'currency' => 'BTC', 'userContainer' => Container::generate('USER', 'secret'), 'operatorContainer' => Container::generate('OPERATOR', 'secret')]);
        $account = $this->client->createAccount($account_to_create);

        $this->assertInstanceOf('\\Ambisafe\\Account\\Wallet4', $account);
        $this->assertEquals(2, $account->getId());
        $this->assertEquals('3MLbmeUjhiU8Artq2Xp3cdKZzXmocQahpy', $account->getAddress());

        $userContainer = json_decode($account->getUserContainer()->toJson(), true);
        $this->assertEquals('USER', $userContainer['role']);
        $this->assertEquals('034a94cacac4327feb793047c514b256b326c3c474d73c861407a8709f9901039e', $userContainer['public_key']);
        $this->assertEquals('ca20faef-ac3f-40a6-99c0-500855c03207', $userContainer['salt']);
        $this->assertEquals('ff55e03b11dc43adf839c3aee3632b36', $userContainer['iv']);
        $this->assertEquals('a0b0cbf2c2697f5141041da8a012149dc4cd82df6f43be8cfc58342ba8e663722178509b667217f2c990ec24ffaeb2ed', $userContainer['data']);

        $operatorContainer = json_decode($account->getOperatorContainer()->toJson(), true);
        $this->assertEquals('OPERATOR', $operatorContainer['role']);
        $this->assertEquals('02f7c53abce8930149ce45e874f4a872d0f27d4fb19b97237c6a816896f32b210c', $operatorContainer['public_key']);
        $this->assertEquals('472de771-7822-438e-847b-3408101290e5', $operatorContainer['salt']);
        $this->assertEquals('3512dfcf7650e127bcdf62dad0c3c0e8', $operatorContainer['iv']);
        $this->assertEquals('a84281c2a5c39dccdcdd19459057a0151373e7bee29f59569fd85d7977bf3162b61862638d12c9199202a0950c1f3263138454599aa39da56e983f6828b24f75d0115e3c96e5aa12d2fcb207551b129c', $operatorContainer['data']);
    }

    public function testGetAccount()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/get_simple_account_response'))
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);
        $account = $this->client->getAccount(1, 'BTC');
        $this->assertEquals(1, $account->getId());
        $this->assertEquals('3MLbmeUjhiU8Artq2Xp3cdKZzXmocQahpy', $account->getAddress());
    }

}
