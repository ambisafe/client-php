<?php
namespace Ambisafe\Tests;

use Ambisafe\Client;
use Ambisafe\Container;
use Ambisafe\Example;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\MessageSigner\MessageSigner;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;


class ClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Ambisafe\Client
     */
    public $client;

    public function setUp()
    {
        $this->client = new Client('http://104.155.86.243:8080', 'ololo', 'demo', 'demo');
    }

    public function testBuildTransaction()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/build_transaction_response'))
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);

        $transaction = $this->client->buildTransaction(1, 'BTC', '39Ccf2Hr2ns58ugt1mRZw7tKA3AhZu41EJ', 1);
        $this->assertTrue($transaction instanceof \Ambisafe\Transaction\Base);
    }

    public function testSubmitTransaction()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/build_transaction_response')),
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/submit_transaction_response')),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);

        $transaction = $this->client->buildTransaction(1, 'BTC', '39Ccf2Hr2ns58ugt1mRZw7tKA3AhZu41EJ', 1);
        $response = $this->client->submitTransaction(1, $transaction, 'BTC');
        $this->assertEquals($response['state'], 'sending');
    }

    public function testGetBalance()
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/../_data/get_balance_response')),
        ]);
        $handler = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handler, 'base_uri' => 'http://localhost:8080']);
        $this->client->setClient($client);

        $this->assertEquals(0.0011086, $this->client->getBalance(1, 'BTC'));
    }

    public function testSignWallet4Transaction()
    {
        $container = Container::generate('OPERATOR', 'secret'); # in real life this would be received from get_account->getOperatorContainer()
        $signatures = $container->sign(['71b173fe61a1ed0b950d2d5eef908866799ae48b9fd3d9546bc2443ac79c4daa'], 'secret');

        $this->assertEquals(1, count($signatures));
    }
}
