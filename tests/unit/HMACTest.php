<?php
namespace Ambisafe\Tests;

use Ambisafe\HMAC;

/**
 * Class HMACTest
 * @package Ambisafe\Tests
 */
class HMACTest extends \PHPUnit_Framework_TestCase
{
    public function testWithDefinedNonceGET()
    {
        $hmac = new HMAC('demo', 'http://ambisafe.co/test', 'GET', null, 1449570179339);
        $this->assertEquals(
            '2wAw4fz2A8YQYcYAdX+H1dxakXj1w1KWB4ImQ0mTA3ldmw8Cl+hzlWWbmzNIvA7HuuvA0EdTFCK6tSL+biVB0Q==',
            $hmac->getSignature()
        );
    }

    public function testWithDefinedNoncePOST()
    {
        $hmac = new HMAC('demo', 'http://ambisafe.co/test', 'POST', '{"myData":"1"}', 1449570179339);
        $this->assertEquals(
            'kMz6TLtyPtMmUIM5YKnxgA5U8c/hc6OZxfPqQng4a6xfoZ/L27Juz/X5/c/Q7YK5ajoRLzX9gLnupsN5y6JS5A==',
            $hmac->getSignature()
        );
    }

    public function testNonceCalculation()
    {
        $this->assertEquals((int)(microtime(true) * 1000), HMAC::calculateNonce(), '', 100);
    }
}
