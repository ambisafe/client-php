# Ambisafe client

## Install

Use composer

    composer require ambisafe/client

## Usage

    See Example.php for all usage examples.
    Don't forget to catch AmbisafeError - it will be thrown on error.

## Disclaimer

The library still in BETA. There can be changes without backward compatibility. 
Account prefixes are in ALPHA state, use with caution.