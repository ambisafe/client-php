<?php
namespace Ambisafe;

use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Crypto\Random\Rfc6979;
use BitWasp\Bitcoin\Key\PrivateKeyFactory;
use BitWasp\Bitcoin\Signature\TransactionSignature;
use BitWasp\Bitcoin\Transaction\SignatureHash\SigHashInterface;
use BitWasp\Buffertools\Buffer;

class Container
{
    protected $_publicKey;
    protected $_data;
    protected $_iv;
    protected $_salt;
    protected $_role;
    /** @const */
    public static $ROLES = ['USER', 'OPERATOR'];

    public function __construct($data)
    {
        $this->_publicKey = Base::valueOrDefault($data, 'publicKey');
        $this->_data = Base::valueOrDefault($data, 'data');
        $this->_iv = Base::valueOrDefault($data, 'iv');
        $this->_salt = Base::valueOrDefault($data, 'salt');
        $this->_role = Base::valueOrDefault($data, 'role');
    }

    /**
     * Generate new private and public key
     */
    public static function generateKeyPair()
    {
        $private = PrivateKeyFactory::create(true);
        $public = $private->getPublicKey();

        return [$private->getBinary(), $public->getBinary()];
    }

    /**
     * Create new class instance with freshly generated key pair, private key is encrypted with $secret
     * @param $role
     * @param $secret
     * @return Container
     * @throws \Exception
     */
    public static function generate($role, $secret)
    {
        if (!in_array($role, self::$ROLES)) {
            throw new \Exception("Bad value of 'role' parameter: {$role}");
        }

        list($privateKey, $publicKey) = static::generateKeyPair();
        $salt = Crypto::generateUuid();
        list ($iv, $encryptedPrivateKey) = Crypto::encrypt($privateKey, $salt, $secret);

        return new Container([
            'publicKey' => Crypto::toHex($publicKey),
            'data' => Crypto::toHex($encryptedPrivateKey),
            'iv' => Crypto::toHex($iv),
            'salt' => $salt,
            'role' => $role
        ]);
    }

    /**
     * Sign an array of hashes
     * @param array $hashes
     * @param $secret
     * @return array
     */
    public function sign(array $hashes, $secret)
    {
        $signatures = [];
        $privateKey = PrivateKeyFactory::fromHex(Crypto::toHex($this->getPrivateKey($secret)));
        $ecAdapter = Bitcoin::getEcAdapter();

        foreach ($hashes as $hash)
        {
            $hash = Buffer::hex($hash);

            $signature = new TransactionSignature(
                $ecAdapter,
                $ecAdapter->sign(
                    $hash,
                    $privateKey,
                    new Rfc6979(
                        $ecAdapter,
                        $privateKey,
                        $hash,
                        'sha256'
                    )
                ),
                SigHashInterface::ALL
            );

            $signatures[] = $signature->getHex();
        }

        return $signatures;
    }

    public function toJson()
    {
        return json_encode($this->asArray());
    }

    public function asArray()
    {
        return [
            'role' => $this->_role,
            'public_key' => $this->_publicKey,
            'salt' => $this->_salt,
            'iv' => $this->_iv,
            'data' => $this->_data
        ];
    }

    /**
     * Decrypt private key and return it
     * @param $secret
     * @return string
     */
    private function getPrivateKey($secret)
    {
        return Crypto::decrypt(Crypto::toBinary($this->_data), $this->_salt, Crypto::toBinary($this->_iv), $secret);
    }

}