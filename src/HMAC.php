<?php
namespace Ambisafe;


class HMAC
{
    protected $_secret;
    protected $_url;
    protected $_method;
    protected $_body;
    protected $_nonce;
    protected $_signature;

    public function __construct($secret, $url, $method, $body='', $nonce=null)
    {
        $this->_secret = $secret;
        $this->_url = $url;
        $this->_method = $method;
        $this->_body = $body;
        $this->_nonce = $nonce;
    }

    public function getSignature()
    {
        if ($this->_signature === null) {
            $message = $this->getNonce() . "\n" . $this->_method . "\n" . $this->_url . "\n" . $this->_body;
            $this->_signature = str_replace("\n", "", base64_encode(hash_hmac('sha512', $message,
                $this->_secret, true)));
        }

        return $this->_signature;
    }

    public static function calculateNonce()
    {
        return (int)(microtime(true) * 1000);
    }

    public function getNonce()
    {
        if ($this->_nonce === null) {
            $this->_nonce = static::calculateNonce();
        }

        return $this->_nonce;
    }
}