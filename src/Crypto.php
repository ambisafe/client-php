<?php
namespace Ambisafe;

class Crypto
{
    const ITERATIONS = 1000;
    const KEY_LENGTH = 32;

    public static function encrypt($data, $salt, $password)
    {
        $key = self::deriveKey($salt, $password);
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);

        return [$iv, $encrypted];
    }

    public static function decrypt($encrypted, $salt, $iv, $password)
    {
        $key = self::deriveKey($salt, $password);
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted, MCRYPT_MODE_CBC, $iv);
        $pad = ord($decrypted[strlen($decrypted) - 1]);
        if ($pad > 0) {
            $decrypted = substr($decrypted, 0, -$pad);
        }

        return $decrypted;
    }

    public static function toHex($data)
    {
        $unpacked = unpack('H*', $data);
        return $unpacked[1];
    }

    public static function toBinary($data)
    {
        return hex2bin($data);
    }

    public static function deriveKey($salt, $password)
    {
        return hash_pbkdf2("sha512", $password, $salt, self::ITERATIONS, self::KEY_LENGTH, true);
    }

    /**
     * Generate valid UUIDv4
     */
    public static function generateUuid()
    {
        $data = openssl_random_pseudo_bytes(16);
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }


}