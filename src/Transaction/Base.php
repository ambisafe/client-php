<?php
namespace Ambisafe\Transaction;

class Base extends \ArrayObject
{

    public function __construct($input = null, $flags = 0, $iterator_class = "ArrayIterator")
    {
        parent::__construct($input, \ArrayObject::STD_PROP_LIST, $iterator_class);
    }

    public function toJson()
    {
        $data = [];
        $iterator = $this->getIterator();
        while ($iterator->valid()) {
            $data[$iterator->key()] = $iterator->current();
            $iterator->next();
        }

        return json_encode($data);
    }

}