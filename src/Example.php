<?php
namespace Ambisafe;

require_once '../vendor/autoload.php';

use Ambisafe\Account\UserSideKey;

class Example
{
    /**
     * @var Client
     */
    private $client;
    private $secret;
    private $wallet4AccountId, $multisigAccountId;

    public function run() {
        # set up required parameters:
        $ambisafeServerUrl = 'http://localhost:8080/';
        $this->secret = 'the key for encryption of OPERATOR keys, keep it in a safe place';
        $apiKey = 'demo';
        $apiSecret = 'demo';

        # those are used for example:
        $accountIdPrefix = 'test-' . Crypto::generateUuid() . '-'; # prefix functionality is still in alpha
        $this->multisigAccountId = 'test-multisig' . random_int(1, PHP_INT_MAX);
        $this->wallet4AccountId = 'test-wallet4' . random_int(1, PHP_INT_MAX);

        $this->client = new Client($ambisafeServerUrl, $this->secret, $apiKey, $apiSecret, $accountIdPrefix);

        $this->createUserSideKey();
        $this->getAccount();
        $this->getAccountBalance();
        $this->sendSimpleTransaction();
    }

    private function getAccount()
    {
        $account = $this->client->getAccount($this->multisigAccountId, 'BTC');
        echo "Account id={$account->getId()}, address={$account->getAddress()}\n";
    }

    private function getAccountBalance()
    {
        $balance = $this->client->getBalance($this->multisigAccountId, 'BTC');
        echo "Account BTC balance={$balance}\n";
    }

    private function createUserSideKey()
    {
        $userContainer = Container::generate('USER', $this->secret);

        $account_to_create = new UserSideKey([
            'id' => $this->multisigAccountId,
            'currency' => 'BTC',
            'userContainer' => $userContainer
        ]);

        $account = $this->client->createAccount($account_to_create);
        echo "Created a UserSideKey account, address: {$account->getAddress()}\n";
    }

    private function sendSimpleTransaction()
    {
        # buildTransaction will fail if source account doesn't have 0.01 BTC
        $account = $this->client->getAccount($this->multisigAccountId, 'BTC');
        /* @var \Ambisafe\Account\UserSideKey $account */
        $transaction = $this->client->buildTransaction(
            $this->multisigAccountId,
            'BTC',
            '18JLtKRA43RoGHcW6sGBfarxVqtqM5STgh',
            0.01
        );
        $account->sign($transaction, 'USER', $this->secret);
        $result = $this->client->submitTransaction($this->multisigAccountId, $transaction, 'BTC');
        $transaction_hash = $result['transactionHash']; # transaction hash that should be saved
        $transaction_state = $result['state']; # == 'sending'
        $transaction = $result['transaction']; # raw transaction data
    }
}

$example = new Example();
$example->run();