<?php
namespace Ambisafe;

class Base
{
    public static function valueOrDefault($data, $parameter, $default = null)
    {
        return isset($data[$parameter]) ? $data[$parameter] : $default;
    }
}