<?php
namespace Ambisafe;

class AmbisafeError extends \Exception {}
class ClientError extends AmbisafeError {}
class ServerError extends AmbisafeError {}
