<?php
namespace Ambisafe\Account;


use Ambisafe\Container;
use Ambisafe\Transaction\Base as BaseTransaction;

/**
 * Class UserSideKey
 * @package Ambisafe\Account
 *
 * @property \Ambisafe\Container $__userContainer
 */
class UserSideKey extends Base
{
    protected $_userContainer;
    protected $_security_schema = 'UserSideKey';

    public function __construct($data)
    {
        $this->_id = self::valueOrDefault($data, 'id');
        $this->_address = self::valueOrDefault($data, 'address');
        $this->_currency = self::valueOrDefault($data, 'currency', 'BTC');
        $this->_userContainer = self::valueOrDefault($data, 'userContainer');
    }

    /**
     * Construct a new UserSideKey account from data returned by Ambisafe Keyserver
     * @param $data
     * @return UserSideKey
     */
    public static function fromServerResponse($data)
    {
        $parsedData = $data['account']; # we want to take all properties from 'account' key by default
        $parsedData['id'] = self::valueOrDefault($data['account'], 'externalId');
        $parsedData['userContainer'] = isset($data['containers']['USER']) ?
            new Container($data['containers']['USER']) : new Container([]);
        return new static($parsedData);
    }

    public function getUserContainer()
    {
        return $this->_userContainer;
    }

    public function toJson()
    {
        return json_encode([
            'id' => $this->_prefix . $this->_id,
            'security_schema' => $this->_security_schema,
            'currency' => $this->_currency,
            'address' => $this->_address, # this field is not required/checked by keyserver
            'containers' => [
                'USER' => $this->_userContainer->asArray()
            ]
        ]);
    }

    /**
     * @param BaseTransaction $transaction
     * @param $role
     * @param $secret
     * @return BaseTransaction
     * @throws \Exception
     */
    public function sign(BaseTransaction $transaction, $role, $secret)
    {
        if (!in_array($role, Container::$ROLES)) {
            throw new \Exception("Bad value of 'role' parameter: {$role}");
        }

        $role = ucfirst(strtolower($role));
        $method = "get{$role}Container";
        $container = call_user_func(array($this, $method));
        /* @var \Ambisafe\Container $container */
        $signedSighashes = $container->sign($transaction['sighashes'], $secret);

        $role = strtolower($role);
        $key = "{$role}_signatures";

        $transaction[$key] = $signedSighashes;

        return $transaction;
    }
}