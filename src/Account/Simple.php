<?php
namespace Ambisafe\Account;


class Simple extends Base
{
    protected $_security_schema = 'Simple';

    public function __construct($data)
    {
        $this->_id = self::valueOrDefault($data, 'id');
        $this->_address = self::valueOrDefault($data, 'address');
        $this->_currency = self::valueOrDefault($data, 'currency', 'BTC');
    }

    /**
     * Construct a new Simple account from data returned by Ambisafe Keyserver
     * @param $data
     * @return Simple
     */
    public static function fromServerResponse($data)
    {
        $parsedData = $data['account']; # we want to take all properties from 'account' key by default
        $parsedData['id'] = self::valueOrDefault($data['account'], 'externalId');
        return new static($parsedData);
    }

    public function toJson()
    {
        return json_encode([
            'id' => $this->_prefix . $this->_id,
            'security_schema' => $this->_security_schema,
            'currency' => $this->_currency,
            'address' => $this->_address # this field is not required/checked by keyserver
        ]);
    }
}
