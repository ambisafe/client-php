<?php
namespace Ambisafe\Account;


abstract class Base extends \Ambisafe\Base
{
    protected $_id;
    protected $_address;
    protected $_currency;
    protected $_prefix;

    public function getId()
    {
        return $this->_id;
    }

    public function getAddress()
    {
        return $this->_address;
    }

    public function setPrefix($prefix)
    {
        $this->_prefix = $prefix;
    }

    public static function fromServerResponse($response)
    {
        $securitySchema = 'Ambisafe\\Account\\' . $response['account']['securitySchemaName'];

        if (!class_exists($securitySchema)) {
            throw new \Exception("Cannot find Account class {$securitySchema}!");
        }

        return $securitySchema::fromServerResponse($response);
    }

    abstract public function toJson();
}