<?php
namespace Ambisafe\Account;


use Ambisafe\Container;
use Ambisafe\Transaction\Base as BaseTransaction;

class Wallet4 extends Base
{
    protected $_userContainer;
    protected $_operatorContainer;
    protected $_security_schema = 'Wallet4';

    public function __construct($data)
    {
        $this->_id = self::valueOrDefault($data, 'id');
        $this->_address = self::valueOrDefault($data, 'address');
        $this->_currency = self::valueOrDefault($data, 'currency', 'BTC');
        $this->_userContainer = self::valueOrDefault($data, 'userContainer');
        $this->_operatorContainer = self::valueOrDefault($data, 'operatorContainer');
    }

    /**
     * Construct a new Wallet4 account from data returned by Ambisafe Keyserver
     * @param $data
     * @return Wallet4
     */
    public static function fromServerResponse($data)
    {
        $parsedData = $data['account']; # we want to take all properties from 'account' key by default
        $parsedData['id'] = self::valueOrDefault($data['account'], 'externalId');
        $parsedData['userContainer'] = isset($data['containers']['USER']) ?
            new Container($data['containers']['USER']) : new Container([]);
        $parsedData['operatorContainer'] = isset($data['containers']['OPERATOR']) ?
            new Container($data['containers']['OPERATOR']) : new Container([]);
        return new static($parsedData);
    }

    public function getUserContainer()
    {
        return $this->_userContainer;
    }

    public function getOperatorContainer()
    {
        return $this->_operatorContainer;
    }

    public function toJson()
    {
        return json_encode([
            'id' => $this->_prefix . $this->_id,
            'security_schema' => $this->_security_schema,
            'currency' => $this->_currency,
            'address' => $this->_address, # this field is not required/checked by keyserver
            'containers' => [
                'USER' => $this->_userContainer->asArray(),
                'OPERATOR' => $this->_operatorContainer->asArray()
            ]
        ]);
    }

    public function sign(BaseTransaction $transaction, $role, $secret)
    {
        if (!in_array($role, Container::$ROLES)) {
            throw new \Exception("Bad value of 'role' parameter: {$role}");
        }

        $role = ucfirst(strtolower($role));
        $method = "get{$role}Container";
        $container = $this->$method();

        $signedSighashes = $container->sign($transaction['sighashes'], $secret);

        $role = strtolower($role);
        $key = "{$role}_signatures";

        $transaction[$key] = $signedSighashes;

        return $transaction;
    }
}