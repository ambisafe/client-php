<?php
namespace Ambisafe;
require_once 'Exceptions.php';

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7;

class Client
{
    protected $_uri;
    protected $_secret;
    protected $_apiKey;
    protected $_apiSecret;
    protected $_accountIdPrefix;

    /**
     * @var GuzzleClient
     */
    protected $_client;

    public function __construct($uri, $secret, $apiKey, $apiSecret, $accountIdPrefix='')
    {
        $this->_uri = $uri;
        $this->_secret = $secret;
        $this->_apiKey = $apiKey;
        $this->_apiSecret = $apiSecret;
        $this->_accountIdPrefix = $accountIdPrefix;

        $this->_client = new GuzzleClient(['base_uri' => $uri]);
    }

    public function setClient(GuzzleClient $client)
    {
        $this->_client = $client;
    }

    public function createAccount(Account\Base $account)
    {
        $account->setPrefix($this->_accountIdPrefix);
        return Account\Base::fromServerResponse($this->makeRequest('POST', "/accounts", $account->toJson()));
    }

    /**
     * @param $id
     * @param $currency
     * @return Account\Base
     * @throws AmbisafeError
     * @throws \Exception
     */
    public function getAccount($id, $currency)
    {
        return Account\Base::fromServerResponse($this->makeRequest(
            'GET',
            "/accounts/{$this->_accountIdPrefix}{$id}/{$currency}"
        ));
    }

    public function getBalance($id, $currency)
    {
        $balance = $this->makeRequest('GET', "/balances/{$currency}/{$this->_accountIdPrefix}{$id}")['balance'];
        if (strrpos($balance, '0x', -strlen($balance)) !== false) {
            return hexdec($balance);
        }
        return (float)$balance;
    }

    public function getBalanceByAddress($address, $currency)
    {
        $balance = $this->makeRequest('GET', "/balances/{$currency}/address/{$address}")['balance'];
        if (strrpos($balance, '0x', -strlen($balance)) !== false) {
            return hexdec($balance);
        }
        return (float)$balance;
    }

    public function buildTransaction($accountId, $currency, $destination, $amount)
    {
        return $this->buildTransactionExtended($accountId, $currency, $destination, $amount, NULL);
    }

    public function buildTransactionExtended($accountId, $currency, $destination, $amount, $additionalArgs)
    {
        $data = [
            'destination' => $destination,
            'amount' => $amount
        ];
        $data = array_merge($data, (array)$additionalArgs);
        $response = $this->makeRequest(
            'POST',
            "/transactions/build/{$this->_accountIdPrefix}{$accountId}/{$currency}",
            json_encode($data)
        );
        return new Transaction\Base($response);
    }

    public function submitTransaction($accountId, Transaction\Base $transaction, $currency)
    {
        return $this->makeRequest(
            'POST',
            "/transactions/submit/{$this->_accountIdPrefix}{$accountId}/{$currency}",
            $transaction->toJson()
        );
    }

    public function makeRequest($method, $uri, $data=null)
    {
        $headers = [];

        if (in_array($method, ['POST', 'PUT'], true)) {
            $headers['Content-Type'] = 'application/json';
        }

        $url = Psr7\Uri::resolve($this->_client->getConfig('base_uri'), $uri);
        $hmac = new HMAC($this->_apiSecret, $url, $method, $data);

        $headers += [
            'Timestamp' => $hmac->getNonce(),
            'Signature' => $hmac->getSignature(),
            'API-key' => $this->_apiKey,
        ];

        $result = json_decode($this->_client->request(
            $method,
            $uri,
            [
                'headers' => $headers,
                'body' => $data,
                'http_errors' => false
            ]
        )->getBody(), true);

        if (isset($result['error']) && !empty($result['error'])) {
            $statusCode = Base::valueOrDefault($result, 'status', '999');
            $errorMessage = Base::valueOrDefault($result, 'status', '999') .
                ' ' .
                $result['error'] .
                ': ' .
                Base::valueOrDefault($result, 'exception', '') .
                ': ' .
                Base::valueOrDefault($result, 'message', '');

            if (400 <= $statusCode && $statusCode < 500) {
                throw new ClientError($errorMessage);
            }
            elseif (500 <= $statusCode && $statusCode < 600) {
                throw new ServerError($errorMessage);
            }
            else {
                throw new AmbisafeError($errorMessage);
            }
        }

        return $result;
    }
}